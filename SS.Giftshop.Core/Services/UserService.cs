﻿using SS.Giftshop.Core.Model.Dtos;
using SS.Giftshop.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Giftshop.Core.Services
{
    public interface IUserService
    {
        UserDto GetById(int id);
    }
    class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public UserDto GetById(int id)
        {
           return _userRepository.GetById(id);
        }
    }
}
