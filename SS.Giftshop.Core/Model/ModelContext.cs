﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Giftshop.Core.Model
{
    public class ModelContext : DbContext
    {
        static ModelContext()
        {
           // System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ModelContext, Migrations.Configuration>());
        }

        public ModelContext()
            : base("AppConnection")
        {
        }

        public ModelContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public ModelContext(DbConnection connection)
            : base(connection, true)
        {
        }

        public IDbSet<Role> Role { get; set; }

        public IDbSet<User> User { get; set; }

        public IDbSet<UserRole> UserRole { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ModelNamespaceConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}
