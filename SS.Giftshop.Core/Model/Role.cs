﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using SS.Model;

namespace SS.Giftshop.Core.Model
{
    public class Role : Entity, IRole<int>
    {
        [Required]
        [StringLength(50)]
        [Display(Name = nameof(AppResources.Name), ResourceType = typeof(AppResources))]
        public virtual string Name { get; set; }

        public virtual ICollection<UserRole> Users { get; set; }
    }
}