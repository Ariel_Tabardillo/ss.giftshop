﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Giftshop.Core.Model.Dtos
{
    public class UserDto
    {
        [Required]
        [StringLength(150)]
        [EmailAddress]
        [Display(Name = nameof(AppResources.Email), ResourceType = typeof(AppResources))]
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = nameof(AppResources.UserName), ResourceType = typeof(AppResources))]
        public string UserName { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }
    }
}
