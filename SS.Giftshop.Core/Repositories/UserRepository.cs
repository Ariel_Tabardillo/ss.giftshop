﻿using SS.Data.EntityFramework;
using SS.Giftshop.Core.Model;
using SS.Giftshop.Core.Model.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Giftshop.Core.Repositories
{
    public interface IUserRepository
    {
        UserDto GetById(int id);
    }

    public class UserRepository :  EfRepositoryBase<User>, IUserRepository
    {
        public UserRepository(IWorkspace workspace) : base(workspace)
        {
        }

        public UserDto GetById(int id)
        {
            var entities = Workspace.Query<User>().Where(x => x.Id == id).Select(x => new UserDto
            {
                Email = x.Email,
                UserName = x.UserName,
                Roles = x.Roles

            }).OrderBy(x => x.UserName).FirstOrDefault();

            return entities;
        }
    }
}
