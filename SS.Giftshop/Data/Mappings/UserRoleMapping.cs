﻿using SS.Giftshop.Core.Model;
using System.Data.Entity.ModelConfiguration;

namespace SS.Giftshop.Data.Mappings
{
    internal sealed class UserRoleMapping : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMapping()
        {
            HasKey(x => new { x.UserId, x.RoleId });
        }
    }
}