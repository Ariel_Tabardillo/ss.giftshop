﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SS.Giftshop.Startup))]

namespace SS.Giftshop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ContainerConfig.ConfigureDependencyResolver(app);

            ConfigureAuth(app);
        }
    }
}
