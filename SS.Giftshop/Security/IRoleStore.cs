using System.Threading.Tasks;
using SS.Giftshop.Core.Model;

namespace SS.Giftshop.Security
{
    public interface IRoleStore
    {
        Task CreateAsync(Role role);
        Task DeleteAsync(Role role);
        Task<Role> FindByIdAsync(int roleId);
        Task<Role> FindByNameAsync(string roleName);
        Task UpdateAsync(Role role);
    }
}