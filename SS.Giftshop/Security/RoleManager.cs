using Microsoft.AspNet.Identity;
using SS.Giftshop.Core.Model;

namespace SS.Giftshop.Security
{
    public class RoleManager : RoleManager<Role, int>, IRoleManager
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }
    }
}