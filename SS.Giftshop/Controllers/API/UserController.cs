﻿using SS.Giftshop.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SS.Giftshop.Controllers.API
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        //[HttpGet]
        //[ActionName("Get")]
        //public IHttpActionResult Get()
        //{
        //    var result = _userService.GetAll().ToList();
        //    if (result == null)
        //        return BadRequest();

        //    return Ok(result);
        //}

        [HttpGet]
        [ActionName("GetById")]
        public IHttpActionResult Get(int id)
        {
            var result = _userService.GetById(id);

            if (result == null)
                return BadRequest();

            return Ok(result);
        }
    }
}